# M223: Punchclock
Dies ist eine Beispielapplikation für das Modul M223.

## Loslegen
Folgende Schritte befolgen um loszulegen:
1. Sicherstellen, dass JDK 12 installiert und in der Umgebungsvariable `path` definiert ist.
1. Ins Verzeichnis der Applikation wechseln und über die Kommandozeile mit `./gradlew bootRun` oder `./gradlew.bat bootRun` starten
1. Unittest mit `./gradlew test` oder `./gradlew.bat test` ausführen.
1. Ein ausführbares JAR kann mit `./gradlew bootJar` oder `./gradlew.bat bootJar` erstellt werden.

Folgende Dienste stehen während der Ausführung im Profil `dev` zur Verfügung:
- REST-Schnittstelle der Applikation: http://localhost:8081
- Dashboard der H2 Datenbank: http://localhost:8081/h2-console

## Beschreibung

Die Applikation ist ein Zeiterfassungssystem. Es können Einträge erstellt werden mit Datum und Uhrzeit.
Diese können auch verwaltet werden (get/create/update/delete).

Es gibt User, diese können auf einer AdminPage verwaltet werden (get/create/update/delete). 
Das Login ist im "/ressources/public/index.html", wenn ein User sich erstmals registriert wird er ins Repository
geschrieben und eingeloggt. Beim Einloggen wird dem User ein Token mitgegeben, jeder Besucher der Website kann
alle Pages aufrufen aber nur die "Login/SignUp" Methoden benutzen. 
Nur Benutzer mit einem Token können auch alle Funktionen nutzen. 

## Starten
Um die Applikation zu starten muss nur die PunchclockApplication gestartet werden, sie befindet sich in
"src/main/java/ch.zli.m223.punchclock/". Jetzt muss nur noch im Browser 'http://localhost:8081' 
aufgerufen werden.
