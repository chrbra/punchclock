const URL = 'http://localhost:8081';
let users = [];

const createUser = (e) => {
    e.preventDefault();
    const formData = new FormData(e.target);
    const user = {
    };
    user['username'] = formData.get('username');
    user['password'] = formData.get('username');

    fetch(`${URL}/users/sign-up`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization' : localStorage.getItem("JWT")
        },
        body: JSON.stringify(user)
    }).then((result) => {
        result.json().then((user) => {
            users.push(user);
            renderUsers();
        });
    });
};

const updateUser = (e) => {
    e.preventDefault();
    const formData = new FormData(e.target);
    const userForm = {};
    userForm['id'] = formData.get('update_id');
    userForm['username'] = formData.get('update_username')
    userForm['password'] = formData.get('update_password')

    fetch(`${URL}/users/${userForm['id']}`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': localStorage.getItem("JWT")
        },
        body: JSON.stringify(userForm)
    }).then((result) => {
        result.json().then((user) => {
            users[userForm['id']-1] = user;
            renderUsers();
        });
    });
}

const deleteUser = (id) => {
    fetch(`${URL}/users/${id}`, {
        method: "DELETE",
        headers: {
            'Authorization' : localStorage.getItem("JWT")
        },
    })
        .then(response => indexUsers());
}

const indexUsers = () => {
    fetch(`${URL}/users`, {
        method: 'GET',
        headers: {
            'Authorization' : localStorage.getItem("JWT")
        },
    }).then((result) => {
        result.json().then((result) => {
            users = result;
            renderUsers();
        });
    });
    renderUsers();
};

const loadData_UpdateForm = (id) => {
    fetch(`${URL}/users/${id}`, {
        method: "GET",
        headers: {
            'Authorization': localStorage.getItem("JWT")
        },
    }).then((result) => {
        result.json().then((user) => {
            let id = document.getElementById("update_id");
            let username = document.getElementById("update_username");

            id.setAttribute("value", user.id);
            username.value = user.username;
        });
    });
}

const createCell = (text) => {
    const cell = document.createElement('td');
    cell.innerText = text;
    return cell;
};

const createDeleteButtonCell = (text, id) => {
    const cell = document.createElement('td');
    const button = document.createElement('button');
    button.setAttribute("onclick", "deleteUser(" + id + ")");
    button.innerText = text;
    cell.append(button);
    return button;
}

const createEditButtonCell = (text, id) => {
    const cell = document.createElement('td');
    const button = document.createElement('button');
    button.setAttribute("onClick", "loadData_UpdateForm(" + id + ")");
    button.innerText = text;
    cell.append(button);
    return button;
}


const renderUsers = () => {
    const display = document.querySelector('#userDisplay');
    display.innerHTML = '';
    users.forEach((user) => {
        const row = document.createElement('tr');
        row.appendChild(createCell(user.id));
        row.appendChild(createCell(user.username));
        row.appendChild(createEditButtonCell("Bearbeiten", user.id));
        row.appendChild(createDeleteButtonCell("Löschen", user.id))
        display.appendChild(row);
    });
};

document.addEventListener('DOMContentLoaded', function () {
    const createUserForm = document.querySelector('#createUserForm');
    createUserForm.addEventListener('submit', createUser);
    indexUsers();
});

document.addEventListener('DOMContentLoaded', function () {
    const createUserForm = document.querySelector('#updateUserForm');
    createUserForm.addEventListener('submit', updateUser);
    indexUsers();
});





