const URL = 'http://localhost:8081';
let entries = [];


const dateAndTimeToDate = (dateString, timeString) => {
    return new Date(`${dateString}T${timeString}`).toISOString();
};

const createEntry = (e) => {
    e.preventDefault();
    const formData = new FormData(e.target);
    const entry = {};
    entry['checkIn'] = dateAndTimeToDate(formData.get('checkInDate'), formData.get('checkInTime'));
    entry['checkOut'] = dateAndTimeToDate(formData.get('checkOutDate'), formData.get('checkOutTime'));

    fetch(`${URL}/entries`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': localStorage.getItem("JWT")
        },
        body: JSON.stringify(entry)
    }).then((result) => {
        result.json().then((entry) => {
            entries.push(entry);
            renderEntries();
        });
    });
};

const updateEntry = (e) => {
    e.preventDefault();
    const formData = new FormData(e.target);
    const entryForm = {};
    entryForm['id'] = formData.get('update_id');
    entryForm['checkIn'] = dateAndTimeToDate(formData.get('checkInDate'), formData.get('checkInTime'));
    entryForm['checkOut'] = dateAndTimeToDate(formData.get('checkOutDate'), formData.get('checkOutTime'));

    fetch(`${URL}/entries/${entryForm['id']}`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': localStorage.getItem("JWT")
        },
        body: JSON.stringify(entryForm)
    }).then((result) => {
        result.json().then((entry) => {
            entries[entryForm['id']-1] = entry;
            renderEntries();
        });
    });
}


const deleteEntries = (id) => {
    fetch(`${URL}/entries/${id}`, {
        method: "DELETE",
        headers: {
            'Authorization': localStorage.getItem("JWT")
        },
    })
        .then(response => indexEntries());
}

const loadData_UpdateForm = (id) => {
    fetch(`${URL}/entries/${id}`, {
        method: "GET",
        headers: {
            'Authorization': localStorage.getItem("JWT")
        },
    }).then((result) => {
        result.json().then((entry) => {
            let id = document.getElementById("update_id");
            let checkIn_Date = document.getElementById("update_checkIn_Date");
            let checkIn_Time = document.getElementById("update_checkIn_Time");
            let checkOut_Date = document.getElementById("update_checkOut_Date");
            let checkOut_Time = document.getElementById("update_checkOut_Time");

            let checkIn_Date_cutted = entry.checkIn.substring(0,10)
            let checkIn_Time_cutted = entry.checkIn.substring(11,19)
            let checkOut_Date_cutted = entry.checkOut.substring(0,10)
            let checkOut_Time_cutted = entry.checkOut.substring(11,19)

            id.setAttribute("value", entry.id);
            checkIn_Date.setAttribute("value", checkIn_Date_cutted);
            checkIn_Time.setAttribute("value", checkIn_Time_cutted);
            checkOut_Date.setAttribute("value", checkOut_Date_cutted);
            checkOut_Time.setAttribute("value", checkOut_Time_cutted);

        });
    });
}

const createCell = (text) => {
    const cell = document.createElement('td');
    cell.innerText = text;
    return cell;
};

const createDeleteButtonCell = (text, id) => {
    const cell = document.createElement('td');
    const button = document.createElement('button');
    button.setAttribute("onclick", "deleteEntries(" + id + ")");
    button.innerText = text;
    cell.append(button);
    return button;
}

const createEditButtonCell = (text, id) => {
    const cell = document.createElement('td');
    const button = document.createElement('button');
    button.setAttribute("onClick", "loadData_UpdateForm(" + id + ")");
    button.innerText = text;
    cell.append(button);
    return button;
}


const renderEntries = () => {
    const display = document.querySelector('#entryDisplay');
    display.innerHTML = '';
    entries.forEach((entry) => {
        const row = document.createElement('tr');
        row.appendChild(createCell(entry.id));
        row.appendChild(createCell(new Date(entry.checkIn).toLocaleString()));
        row.appendChild(createCell(new Date(entry.checkOut).toLocaleString()));
        row.appendChild(createEditButtonCell("Bearbeiten", entry.id));
        row.appendChild(createDeleteButtonCell("Löschen", entry.id));
        display.appendChild(row);
    });
};


const indexEntries = () => {
    fetch(`${URL}/entries`, {
        method: 'GET',
        headers: {
            'Authorization': localStorage.getItem("JWT")
        },
    }).then((result) => {
        result.json().then((result) => {
            entries = result;
            renderEntries();
        });
    });
    renderEntries();
};


document.addEventListener('DOMContentLoaded', function () {
    const createEntryForm = document.querySelector('#createEntryForm');
    createEntryForm.addEventListener('submit', createEntry);
    indexEntries();
});

document.addEventListener('DOMContentLoaded', function () {
    const createEntryForm = document.querySelector('#updateEntryForm');
    createEntryForm.addEventListener('submit', updateEntry);
    indexEntries();
});



