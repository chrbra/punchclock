const  URL = 'http://localhost:8081';
let showLogin = true;

const login = (e) => {
    e.preventDefault();
    const formData = new FormData(e.target);
    const cred = {};
    cred['username'] = formData.get('username');
    cred['password'] = formData.get('password');

    fetch(`${URL}/login`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(cred)

    }).then((result) => {
        if(result.status === 200){
            localStorage.setItem("JWT", result.headers.get('Authorization'));
            window.location.replace(`${URL}/homePage.html`);
        } else {
            return null;
        }
    });
}

const validatePasswordSignUp = (e) => {
    e.preventDefault();
    const formData = new FormData(e.target);
    const cred = {};
    cred['password'] = formData.get('password');
    cred['confirm_password'] = formData.get('confirm_password');

    if (cred['password'] !== cred['confirm_password']){
        alert("Passwords are not equal!");
    } else {
       signUp(e);
    }
}

const signUp = (e) => {
    e.preventDefault();
    const formData = new FormData(e.target);
    const cred = {};
        cred['username'] = formData.get('username');
        cred['password'] = formData.get('password');

    fetch(`${URL}/users/sign-up`,{
        method: 'POST',
        headers:{
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(cred)
    }).then((result) =>{
        if(result.status === 200){
            login(e);
        } else {
            return null;
        }
    });
}


document.addEventListener('DOMContentLoaded', function (){
   const signInForm = document.querySelector('#signInForm');
   signInForm.addEventListener('submit', (e) => login(e));
});

document.addEventListener('DOMContentLoaded', function (){
    const signUpForm = document.querySelector('#signUpForm');
    signUpForm.addEventListener('submit', (e) => validatePasswordSignUp(e));
});




document.addEventListener('DOMContentLoaded', function (){
    const switchAuthentication = document.querySelector('#hello');
    switchAuthentication.addEventListener('click', () => switchAuth());
});


const switchAuth = () => {

    let loginForm = document.getElementById('signInForm');
    let signUpForm = document.getElementById('signUpForm');
    let button = document.getElementById('hello')

   showLogin = !showLogin;

    if (showLogin === true) {
        signUpForm.classList.add("display_none");
        loginForm.classList.remove("display_none");
        button.innerText = 'Sign Up?';
    } else {
        signUpForm.classList.remove("display_none");
        loginForm.classList.add("display_none");
        button.innerText = 'Sign In?';
    }
}