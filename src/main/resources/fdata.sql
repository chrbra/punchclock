insert into role (name) values
('admin'),
('user');

insert into category (name) values
('category_one'),
('category_two');

insert into user (username, password, role_id) values
('Chris', '12345', 1),
('Max', '12345', 2);

