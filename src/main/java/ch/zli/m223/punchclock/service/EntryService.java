package ch.zli.m223.punchclock.service;

import ch.zli.m223.punchclock.domain.Entry;
import ch.zli.m223.punchclock.repository.EntryRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EntryService {
    private EntryRepository entryRepository;

    public EntryService(EntryRepository entryRepository) {
        this.entryRepository = entryRepository;
    }

    public Entry saveEntry(Entry entry) {
        return entryRepository.saveAndFlush(entry);
    }

    public Entry getEntryById(Long id){
        return entryRepository.findById(id).get();
    }

    public void deleteEntryById(Long id) {
        if (entryRepository.existsById(id)) {
            entryRepository.deleteById(id);
        }
    }

//    public Entry updateEntry(Entry entry){
//        if (entryRepository.existsById(entry.getId())){
//            return entryRepository.save(entry);
//        }
//        return null;
//    }


    public List<Entry> findAll() {
        return entryRepository.findAll();
    }
}
