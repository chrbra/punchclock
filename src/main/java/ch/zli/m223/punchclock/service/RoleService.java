//package ch.zli.m223.punchclock.service;
//
//import ch.zli.m223.punchclock.domain.Role;
//import ch.zli.m223.punchclock.repository.RoleRepository;
//import org.springframework.stereotype.Service;
//
//import java.util.List;
//
//@Service
//public class RoleService {
//    public final RoleRepository roleRepository;
//
//    public RoleService(RoleRepository roleRepository) {
//        this.roleRepository = roleRepository;
//    }
//
//    public Role createRole(Role role) {
//        return roleRepository.saveAndFlush(role);
//    }
//
//    public Role getRoleById(Long id) {
//        return roleRepository.getOne(id);
//    }
//
//    public List<Role> findAll() {
//        return roleRepository.findAll();
//    }
//
//    public void delete(Long id) {
//        if (roleRepository.existsById(id)) {
//            roleRepository.deleteById(id);
//        }
//    }
//}
