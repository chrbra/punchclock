package ch.zli.m223.punchclock.security;

public class SecurityConstants {
    public static final String SECRET = "SecretKeyToGenJWTs";
    public static final long EXPIRATION_TIME = 864_000_000; // 10 days
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";

    public static final String Root_URL = "/";
    public static final String GET_INDEX_HTML_URL = "/index.html";
    public static final String GET_INDEX_JS_URL = "/index.js";
    public static final String LOGIN_URL = "/login";
    public static final String SIGN_UP_URL = "/users/sign-up";

    public static final String GET_HOMEPAGE_HTML_URL = "/homePage.html";
    public static final String GET_HOMEPAGE_JS_URL = "/homePage.js";

    public static final String GET_ADMINPAGE_HTML_URL = "/adminPage.html";
    public static final String GET_ADMINPAGE_JS_URL = "/adminPage.js";


    public static final String FAVICON = "/favicon.ico";
    public static final String STYLES = "/style.css";


}
