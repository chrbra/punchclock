package ch.zli.m223.punchclock.controller;

import ch.zli.m223.punchclock.domain.Entry;
import ch.zli.m223.punchclock.domain.User;
import ch.zli.m223.punchclock.repository.UserRepository;

import ch.zli.m223.punchclock.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    private UserService userService;
    private UserRepository userRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserController(UserService userService, UserRepository userRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userService = userService;
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    // Gibt eine Liste aller User zurück "/users"
    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    public List<User> getAllUsers(){
        return userService.findAll();
    }

    // Es wird eine ID mitgegeben "/users/{id}", mit dieser wird nach einem bestimmten User gesucht. Der User wird zurückgegeben.
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public User getUserById(@PathVariable Long id){
        return userService.getUserById(id);
    }

    // Es wird ein User registriert, das Passwort wird verschlüsselt und er wird ins Repository gespeichert. Der User wird zurückgegeben
    @PostMapping("/sign-up")
    public User signUp(@RequestBody User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userRepository.save(user);
        return user;
    }

    // Es wird ein User geupdated, die ID des zu editierenden Users wird mitgegeben. Im Repository wird der User überschrieben
    // Es wird kein neuer erstellt
    @PostMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public User update(@RequestBody User user,  @PathVariable Long id) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setId(id);
        userRepository.save(user);
        return user;
    }

    // Es wird eine ID mitgegeben und der User mit dieser ID im Repository wird gelöscht. Es wird nichts zurückgegeben
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteEntry(@PathVariable Long id) {
        userService.deleteUserById(id);
    }
}