package ch.zli.m223.punchclock.controller;

import ch.zli.m223.punchclock.domain.Entry;
import ch.zli.m223.punchclock.service.EntryService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/entries")
public class EntryController {
    private EntryService entryService;

    public EntryController(EntryService entryService) {
        this.entryService = entryService;
    }

    // Gibt eine Liste aller Entries zurück "/entries"
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Entry> getAllEntries() {
        return entryService.findAll();
    }

    // Es wird eine ID mitgegeben "/entries/{id}", mit dieser wird nach einem bestimmten Entry gesucht. Der Entry wird zurückgegeben.
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Entry getEntryById(@PathVariable Long id){
        return entryService.getEntryById(id);
    }

    // Erstellt einen Entry. Das ID Attribut des mitgegebenen Entry ist leer, somit wird er im Repository erstellt.
    // Ein Entry wird zurückgegeben
    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public Entry createEntry(@Valid @RequestBody Entry entry) {
        return entryService.saveEntry(entry);
    }

    // Updated einen Entry. Das ID Attribut des mitgegebenen Entry ist leer, es wird die ID von dem zu editierenden Entry mitgegeben.
    // Somit wird der vorhandene Entry im Repository überschrieben und es wird kein neuer erstellt.Gibt einen Entry zurück
    @PostMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Entry updateEntry(@Valid @RequestBody Entry entry, @PathVariable Long id) {
        entry.setId(id);
        return entryService.saveEntry(entry);
    }

    // Löscht einen Entry. Es wird eine ID mitgegeben und diese wird verwendet um im Repository einen bestimmten Datensatz zu löschen.
    // Es wird nichts zurückgegeben
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteEntry(@PathVariable Long id) {
        entryService.deleteEntryById(id);
    }
}
